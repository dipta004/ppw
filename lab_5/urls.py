from django.urls import path

from . import views

app_name="lab5"

urlpatterns=[
    path("", views.index, name="index" ),
    path("delete/<int:id>", views.delete_schedule, name="delete_schedule")
]