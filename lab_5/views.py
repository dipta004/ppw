import datetime

from django.shortcuts import render, redirect
from . import form,models
# Create your views here.
def index(request):
    if(request.method == "POST"):
            forms = form.ScheduleForm(request.POST);
            if(forms.is_valid()):
                schedule = models.Schedule(title=request.POST['title'],
                                           location=request.POST['location'],
                                           date="{}-{:02d}-{:02d}".format(request.POST['date_year'],int(request.POST['date_month']),int(request.POST['date_day'])),
                                           categories=request.POST['categories'],
                                           time=request.POST['time'],
                                           day=request.POST['day']
                                           )
                schedule.save()
                return redirect('/lab_5')
            else:
                list_schedule = models.Schedule.objects.all().order_by('date').order_by('time')
                return render(request, 'lab_5/schedule.html', {'forms':forms, 'schedules':list_schedule, "length":len(list_schedule)})
    else:
        forms = form.ScheduleForm()
        list_schedule = models.Schedule.objects.all().order_by('date').order_by('time')
        return render(request, 'lab_5/schedule.html', {'forms':forms, 'schedules':list_schedule, "length":len(list_schedule)})

def delete_schedule(request,id):
    if(request.method == "POST"):
        schedule = models.Schedule.objects.get(id=id)
        schedule.delete()
        return redirect('/lab_5')