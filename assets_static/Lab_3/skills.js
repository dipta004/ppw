let skillDOM = document.getElementById("skill-lists");

const SkillLists = [{
    img:"/static/iconfinder_React.js_logo_1174949.svg",
    name:"ReactJS"
},{
    img:"/static/iconfinder_java_386470.svg",
    name:"Java"
},{
    img:"/static/iconfinder_js_282802.svg",
    name:"JavaScript"
},{
    img:"/static/iconfinder_267_Python_logo_4375050.svg",
    name:"Python"
},{
    img:"/static/ruby-seeklogo.com.svg",
    name:"Ruby"
},{
    img:"/static/django-seeklogo.com.svg",
    name:"Django"
},{
    img:"/static/Play.svg",
    name:"Play"
},{
    img:"/static/iconfinder_144_Gitlab_logo_logos_4373151.svg",
    name:"Gitlab"
}];

skillDOM.innerHTML += /*html*/`
    ${SkillLists.map((obj,index) => {
        return /*html*/`
            <div class="mx-3 my-2" style="display: block;" >
                <div class="text-center">
                    <img src=${obj.img} alt="" class="img-fluid" style="max-height: 100px;"/>
                    <h3 class="bold align-self-center mt-3">${obj.name}</h3>
                </div>
            </div>
        `
    }).join("")}
`