const Scroll = new SmoothScroll('a[href*="#"]', {
    speed: 800,
    offset: 200
});

var sw = new ScrollWatch({
        watch: '.part',
        watchOnce: false,
        onElementInView: ({el}) => {
            let id = el.id;
            document.getElementById(id+"-a").classList.add("active");
            document.getElementById(id+"-title").classList.add("title-view");
            document.getElementById(id+"-subtitle").style = "opacity: 1; transform: translateX(0px);"
        },
        onElementOutOfView :({el}) => {
            let id = el.id;
            document.getElementById(id+"-a").classList.remove("active");
            document.getElementById(id+"-title").classList.remove("title-view");
            document.getElementById(id+"-subtitle").style = "";
        }
    });

var sw2 = new ScrollWatch({
    watch: ".landing-page",
    watchOnce: false,
    onElementInView: (data) => {
        let activeList = document.getElementsByClassName("active");
        for(var i = 0; i< activeList.length; i++){
            activeList[i].classList.remove("active");
        }
    }
})

let skillWrapperDOM = document.getElementById("skill-wrapper");
const modifyClass = (query) => {
    if(query.matches){
        skillWrapperDOM.classList.remove("col-9");
    }else{
        skillWrapperDOM.classList.add("col-9");
    }
}
if(matchMedia){
    let mediaQuery = window.matchMedia("(max-width: 500px)");
    mediaQuery.addListener(modifyClass);
    modifyClass(mediaQuery);
}

const active = (element) => {
    let activeList = document.getElementsByClassName("active");
    for(var i = 0; i< activeList.length; i++){
        activeList[i].classList.remove("active");
    }
    element.classList.add("active");
}