from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'lab_4/profile.html');

def not_found(request):
    return render(request, 'lab_4/notfound.html')

def experience(request):
    return render(request, 'lab_4/experience.html')