from django.urls import path
from . import views

urlpatterns = [
    path('',views.index),
    path('not_found/',views.not_found),
    path('experience/',views.experience)
]